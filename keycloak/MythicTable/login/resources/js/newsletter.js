const formID = "kc-register-form";
const newsletterCheckbox = "user.attributes.newsletter";
const hiddenNewsletterCheckbox = "user.attributes.newsletterHidden";

beforeSubmit = function() {
    if(document.getElementById(newsletterCheckbox).checked) {
        document.getElementById(hiddenNewsletterCheckbox).disabled = true;
    }
    document.getElementById(formID).submit();
}