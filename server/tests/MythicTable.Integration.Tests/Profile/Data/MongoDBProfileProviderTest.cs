﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Moq;
using MythicTable.Profile.Data;
using MythicTable.Profile.Exceptions;
using Xunit;

namespace MythicTable.Integration.Tests.Profile.Data
{
    public class MongoDBProfileProviderTest
    {
        private const string UserId = "test-user";
        private const string SecondUserId = "other-user";

        public Mock<ILogger<InMemoryProfileProvider>> LoggerMock;

        private readonly InMemoryProfileProvider provider;

        public MongoDBProfileProviderTest()
        {
            LoggerMock = new Mock<ILogger<InMemoryProfileProvider>>();
            provider = new InMemoryProfileProvider(LoggerMock.Object);
        }

        [Fact]
        public async Task CreatesAssignsId()
        {
            await provider.Create(new ProfileDto(), UserId);
            var profile = await provider.Get(UserId);
            Assert.Equal(UserId, profile.UserId);
        }

        [Fact]
        public async Task CreatesGeneratesId()
        {
            await provider.Create(new ProfileDto(), UserId);
            var profile = await provider.Get(UserId);
            Assert.NotNull(profile.Id);
        }

        [Fact]
        public async Task CreatesGeneratesUniqueId()
        {
            var profile1 = await provider.Create(new ProfileDto(), UserId);
            var profile2 = await provider.Create(new ProfileDto(), SecondUserId);
            Assert.NotEqual(profile1.Id, profile2.Id);
        }

        [Fact]
        public async Task CanDelete()
        {
            await provider.Create(new ProfileDto(), UserId);
            var profile = await provider.Get(UserId);
            await provider.Delete(profile.UserId);
            await Assert.ThrowsAsync<ProfileNotFoundException>(() => provider.Get(UserId));
        }

        [Fact]
        public async Task GetThrowsWhenProfileNotFound()
        {
            var exception = await Assert.ThrowsAsync<ProfileNotFoundException>(() => provider.Get(UserId));
            Assert.Equal($"Cannot find user: {UserId}", exception.Message);
            VerifyLog(LogLevel.Error, $"Cannot find user: {UserId}");
        }

        [Fact]
        public async Task FailedDeleteLogsTheFailure()
        {
            var exception = await Assert.ThrowsAsync<ProfileNotFoundException>(() => provider.Delete(UserId));
            Assert.Equal($"Profile for user: {UserId} doesn't exist", exception.Message);
            VerifyLog(LogLevel.Error, $"Cannot find user: {UserId}");
        }

        [Fact]
        public async Task FailedUpdateLogsTheFailure()
        {
            var exception = await Assert.ThrowsAsync<ProfileInvalidException>(() => provider.Update(null));
            Assert.Equal("The profile is null", exception.Message);
            VerifyLog(LogLevel.Error, "The profile is null");
        }

        [Fact]
        public async Task UpdateRequiresProfileWithUserId()
        {
            var exception = await Assert.ThrowsAsync<ProfileInvalidException>(() => provider.Update(new ProfileDto()));
            Assert.Equal("The profile MUST have an id", exception.Message);
            VerifyLog(LogLevel.Error, "The profile MUST have an id");
        }

        private void VerifyLog(LogLevel expectedLevel, string expected)
        {
            LoggerMock.Verify(l => l.Log(
                It.Is<LogLevel>(level => level == expectedLevel),
                It.IsAny<EventId>(),
                It.Is<It.IsAnyType>((v, t) => v.ToString() == expected),
                It.IsAny<Exception>(),
                It.Is<Func<It.IsAnyType, Exception, string>>((v, t) => true)));
        }
    }
}
