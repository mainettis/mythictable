using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using MythicTable.Integration.Tests.Util;
using MythicTable.Profile.Data;
using Newtonsoft.Json;
using Xunit;

namespace MythicTable.Integration.Tests.Profile.API
{
    public class ProfileTest
    {
        private readonly HttpClient client;

        public ProfileTest()
        {
            var builder = new WebHostBuilder().UseStartup<TestStartup>();
            var server = new TestServer(builder);
            client = server.CreateClient();
        }

        [Fact]
        public async void BasicFlow()
        {
            var profile = new ProfileDto
            {
                DisplayName = "Test"
            };

            
            var result = await Me();

            result.ImageUrl = "http://example.com/test.png";

            await Put(result);

            var allProfiles = await Get(new []{ "Test user" });
            Assert.Single(allProfiles);
            Assert.Equal("http://example.com/test.png", allProfiles.First().ImageUrl);
        }

        [Fact]
        public async void MePopulatesUserId()
        {
            var result = await Me();
            Assert.Equal("Test user", result.UserId);
        }

        [Fact]
        public async void UpdateRequiresSameUserIs()
        {
            var profile = new ProfileDto
            {
                UserId = "Unknown user",
                DisplayName = "Test"
            };
            using var response = await RequestHelper.PutStreamAsync(client, $"/api/profiles", profile);
            Assert.Equal(HttpStatusCode.Forbidden, response.StatusCode);
        }

        private async Task<ProfileDto> Me()
        {
            using var response = await RequestHelper.GetStreamAsync(client, "/api/profiles/me");
            response.EnsureSuccessStatusCode();
            Assert.Equal("application/json; charset=utf-8", response.Content.Headers.ContentType.ToString());
            var json = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<ProfileDto>(json);
        }

        private async Task<List<ProfileDto>> Get(string[] ids)
        {
            var queryString = ToQueryString(ids);
            using var response = await RequestHelper.GetStreamAsync(client, $"/api/profiles{queryString}");
            response.EnsureSuccessStatusCode();
            Assert.Equal("application/json; charset=utf-8", response.Content.Headers.ContentType.ToString());
            var json = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<List<ProfileDto>>(json);
        }

        private async Task<ProfileDto> Put(ProfileDto dto)
        {
            using var response = await RequestHelper.PutStreamAsync(client, $"/api/profiles", dto);
            response.EnsureSuccessStatusCode();
            Assert.Equal("application/json; charset=utf-8", response.Content.Headers.ContentType.ToString());
            var json = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<ProfileDto>(json);
        }

        private string ToQueryString(string[] ids)
        {
            var parameters = ids.Select(id => $"userId={HttpUtility.UrlEncode(id)}");
            return "?" + string.Join("&", parameters);
        }
    }
}