using System.Threading.Tasks;
using MythicTable.Integration.Tests.Util;
using MythicTable.Campaign.Data;
using Newtonsoft.Json;
using Xunit;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;

namespace MythicTable.Integration.Tests
{
    public class CampaignTests
    {
        [Fact]
        public async Task CreateCampaignTest()
        {
            var builder = new WebHostBuilder().UseStartup<TestStartup>();
            var server = new TestServer(builder);
            var client = server.CreateClient();

            var campaign = new CampaignDTO()
            {
                Name = "Integration Test Campaign"
            };

            using var response = await RequestHelper.PostStreamAsync(client, "/api/campaigns", campaign);
            response.EnsureSuccessStatusCode();
            Assert.Equal("application/json; charset=utf-8", response.Content.Headers.ContentType.ToString());
            var json = await response.Content.ReadAsStringAsync();
            var newCampaign = JsonConvert.DeserializeObject<CampaignDTO>(json);
            Assert.Equal("Integration Test Campaign", newCampaign.Name);
        }
    }
}