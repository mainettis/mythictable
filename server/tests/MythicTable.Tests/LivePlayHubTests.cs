using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Moq;
using MythicTable.Campaign.Data;
using MythicTable.Campaign.Util;
using MythicTable.Collections.Data;
using MythicTable.Collections.Providers;
using MythicTable.GameSession;
using Newtonsoft.Json.Linq;
using Xunit;

namespace MythicTable.Tests.GameSessionTests
{
    // TODO #6: Test GameState
    public class LivePlayHubTests
    {
        private Mock<ICampaignProvider> campaignProviderMock;
        private Mock<ICollectionProvider> collectionProviderMock;
        private Mock<ILogger<LivePlayHub>> loggerMock;
        private Mock<IHubCallerClients<ILiveClient>> clientsMock;
        private Mock<ILiveClient> allClientsMock;

        private LivePlayHub hub;

        private string User { get; set; } = "test-user";

        public LivePlayHubTests()
        {
            campaignProviderMock = new Mock<ICampaignProvider>();
            collectionProviderMock = new Mock<ICollectionProvider>();
            loggerMock = new Mock<ILogger<LivePlayHub>>();
            clientsMock = new Mock<IHubCallerClients<ILiveClient>>();
            allClientsMock = new Mock<ILiveClient>();
            
            clientsMock.Setup(m => m.All).Returns(allClientsMock.Object);
            allClientsMock
                .Setup(a => a.ConfirmDelta(It.IsAny<CharacterDelta>()))
                .Returns(Task.CompletedTask);
            allClientsMock
                .Setup(a => a.SendMessage(It.IsAny<MessageDto>()))
                .Returns(Task.CompletedTask);
            allClientsMock
                .Setup(a => a.CharacterAdded(It.IsAny<CharacterDTO>()))
                .Returns(Task.CompletedTask);
            allClientsMock
                .Setup(a => a.ObjectAdded(It.IsAny<string>(), It.IsAny<JObject>()))
                .Returns(Task.CompletedTask);
            allClientsMock
                .Setup(a => a.ObjectUpdated(It.IsAny<UpdateCollectionHubParameters>()))
                .Returns(Task.CompletedTask);

            hub = new LivePlayHub(campaignProviderMock.Object, collectionProviderMock.Object, new ConfigurationBuilder().Build(), loggerMock.Object);
            
            hub.Clients = clientsMock.Object;

            var hubCallerContextMock = new Mock<HubCallerContext>();
            hubCallerContextMock.Setup(c => c.User.FindFirst(It.IsAny<string>()))
                           .Returns(() => {
                               return new Claim("", User);
                           });
            hub.Context = hubCallerContextMock.Object;
        }

        [Fact]
        public async Task ValidMessagesAreExecuted()
        {
            var roll = new MessageDto
            {
                ClientId = "123",
                SessionId = "test",
                Timestamp = 5678,
                Message = "1d5"
            };

            await hub.SendMessage(roll);

            allClientsMock.Verify(
                c => c.SendMessage(It.Is<MessageDto>(rolled => rolled.Result != null && rolled.ClientId == "123" && rolled.SessionId == "test" && rolled.Timestamp == 5678)),
                Times.Once());
        }

        [Fact]
        public async Task MovesCharacter()
        {
            campaignProviderMock
                .Setup(cp => cp.UpdateCharacter(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<JsonPatchDocument>()))
                .Returns(Task.FromResult(1L));
            
            var submittedStep = new CharacterDelta
            {
                CampaignId = "test-campaign",
                Entities = new[]
                {
                    new EntityOperation
                    {
                        EntityId = "character1",
                        Patch = new JsonPatchDocument().Add("/token/pos", JObject.Parse("{'q': 1, 'r': 2}")),
                    },
                },
            };

            await hub.UpdateCharacter(submittedStep);

            campaignProviderMock.Verify(
                c => c.UpdateCharacter(
                    It.Is<string>(campaignId => campaignId == submittedStep.CampaignId),
                    It.Is<string>(id => id == "character1"),
                    It.IsAny<JsonPatchDocument>()),
                Times.Once);
        }

        [Fact]
        public async Task AddCharacter()
        {
            var request = new AddCharacterRequest
            {
                campaignId = "test-campaign",
                x = 1,
                y = 2,
                image = "https://example.com/character.png"
            };
            var character = CharacterUtil.CreateCharacter("scene", request.image, request.x, request.y);

            campaignProviderMock
                .Setup(cp => cp.AddCharacter(It.IsAny<string>(), It.IsAny<CharacterDTO>()))
                .Returns(Task.FromResult(character));

            await hub.AddCharacter(request);

            campaignProviderMock.Verify(
                c => c.AddCharacter(
                    It.Is<string>(campaignId => campaignId == request.campaignId),
                    It.Is<CharacterDTO>(dto =>
                        dto.Token["pos"]["q"] == request.x &&
                        dto.Token["pos"]["r"] == request.y &&
                        dto.Asset["src"] == request.image
                    )),
                Times.Once);

            allClientsMock.Verify(
                c => c.CharacterAdded(
                    It.Is<CharacterDTO>(dto =>
                        dto.Token["pos"]["q"] == request.x &&
                        dto.Token["pos"]["r"] == request.y &&
                        dto.Asset["src"] == request.image
                    )),
                Times.Once);
        }

        [Fact]
        public async Task RemoveCharacterTest()
        {
            campaignProviderMock.Setup(cp => cp.RemoveCharacter(It.IsAny<string>(), It.IsAny<string>()));
            
            var request = new RemoveCharacterRequest
            {
                CampaignId = "test-campaign",
                CharacterId = "test-character",
            };

            await hub.RemoveCharacter(request);

            campaignProviderMock.Verify(
                c => c.RemoveCharacter(
                    It.Is<string>(campaignId => campaignId == request.CampaignId),
                    It.Is<string>(characterId => characterId == request.CharacterId)),
                Times.Once);
        }

        [Fact]
        public async Task AddCollectionItem()
        {
            var item = new JObject();
            collectionProviderMock
                .Setup(cp => cp.CreateByCampaign(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<JObject>()))
                .Returns(Task.FromResult(item));

            await hub.AddCollectionItem("characters", "campaign-id", item);

            collectionProviderMock.Verify(cp => cp.CreateByCampaign(
                It.IsAny<string>(),
                It.IsAny<string>(),
                It.IsAny<string>(),
                It.IsAny<JObject>()), Times.Once);
            allClientsMock.Verify(a => a.ObjectAdded("characters", It.IsAny<JObject>()));
        }

        [Fact]
        public async Task UpdateObject()
        {
            collectionProviderMock
                .Setup(cp => cp.UpdateByCampaign(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<JsonPatchDocument>()))
                .Returns(Task.FromResult(1));
            var patch = new JsonPatchDocument().Add("/token/pos", JObject.Parse("{'q': 1, 'r': 2}"));

            await hub.UpdateObject(new UpdateCollectionHubParameters
            {
                Collection = "characters",
                CampaignId = "campaign-id",
                Id = "obj-id",
                Patch = patch
            });

            collectionProviderMock.Verify(
                c => c.UpdateByCampaign(
                    It.Is<string>(collection => collection == "characters"),
                    It.Is<string>(campaignId => campaignId == "campaign-id"),
                    It.Is<string>(id => id == "obj-id"),
                    It.Is<JsonPatchDocument>(p => p == patch)),
                Times.Once);
            allClientsMock.Verify(a => a.ObjectUpdated(
                    It.Is<UpdateCollectionHubParameters>(p => p.Id == "obj-id" &&  p.Patch == patch)),
                Times.Once);
        }

        [Fact]
        public async Task RemoveObject()
        {
            collectionProviderMock
                .Setup(cp => cp.Delete(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(Task.FromResult(1));

            await hub.RemoveObject("characters", "obj-id");

            collectionProviderMock.Verify(
                c => c.Delete("test-user", "characters", "obj-id"),
                Times.Once);
            allClientsMock.Verify(a => a.ObjectRemoved("characters", "obj-id"));
        }

        [Fact]
        public async Task RemoveObjectDoesNotNotifyOnError()
        {
            collectionProviderMock
                .Setup(cp => cp.Delete(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(Task.FromResult(0));

            var results = await hub.RemoveObject("characters", "obj-id");

            Assert.False(results);
            collectionProviderMock.Verify(
                c => c.Delete("test-user", "characters", "obj-id"),
                Times.Once);
            allClientsMock.Verify(a => a.ObjectRemoved(It.IsAny<string>(), It.IsAny<string>()), Times.Never);
        }
    }
}
