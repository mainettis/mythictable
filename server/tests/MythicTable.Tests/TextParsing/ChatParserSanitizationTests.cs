﻿using MythicTable.TextParsing;
using Xunit;

namespace MythicTable.Tests.TextParsing
{
    public class ChatParserSanitizationTests
    {
        private readonly ChatParser parser;

        public ChatParserSanitizationTests()
        {
            parser = new ChatParser();
        }

        [Fact]
        public void ProcessDoesNotSanitize()
        {
            var results = parser.Process("This is simple text.<br/>");
            Assert.Equal("This is simple text.<br/>", results.Text);
        }

        [Fact]
        public void ConvertingToDtoSanitizes()
        {
            var results = parser.Process("This is simple text.<br/>").AsDto();
            Assert.Equal("This is simple text.&lt;br/&gt;", results.Message);
        }

        [Fact]
        public void ChatDtoSanitizes()
        {
            var dto = new ChatDto();
            dto.Message = "<br/>";
            Assert.Equal("&lt;br/&gt;", dto.Message);
        }

        [Fact]
        public void ChatDtoObjectInitializationSanitizes()
        {
            var dto = new ChatDto {Message = "<br/>"};
            Assert.Equal("&lt;br/&gt;", dto.Message);
        }

        [Fact]
        public void IgnoresSimpleText()
        {
            var dto = new ChatDto { Message = "This is simple text." };
            Assert.Equal("This is simple text.", dto.Message);
        }

        [Fact]
        public void SanitizesDescription()
        {
            var dto = new ChatDto { Description = "<br/>" };
            Assert.Equal("&lt;br/&gt;", dto.Description);
        }

        [Fact]
        public void SanitizesCommonDescriptionElements()
        {
            var dto = new ChatDto { Description = "1d6 => 1! => 1" };
            Assert.Equal("1d6 =&gt; 1! =&gt; 1", dto.Description);
        }
    }
}
