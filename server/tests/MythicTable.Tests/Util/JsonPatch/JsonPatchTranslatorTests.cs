using MongoDB.Bson;
using MythicTable.Common.JsonPatch;
using Newtonsoft.Json.Linq;
using Xunit;

namespace MythicTable.Tests.Util.JsonPatch
{
    public class JsonPatchTranslatorTests
    {
        private readonly JsonPatchTranslator translator;

        public JsonPatchTranslatorTests()
        {
            translator = new JsonPatchTranslator();
        }

        [Fact]
        public void TestJsonPath2MongoPath()
        {
            var mongoPath = translator.JsonPath2MongoPath("/foo/bar");
            Assert.Equal("foo.bar", mongoPath);
        }

        [Fact]
        public void TestJsonPath2MongoPathForTokens()
        {
            var mongoPath = translator.JsonPath2MongoCharacterPath("/token/pos");
            Assert.Equal("Characters.$.Token.pos", mongoPath);
        }

        [Fact]
        public void TestJsonPath2MongoPathHandlesTrailingSlash()
        {
            var mongoPath = translator.JsonPath2MongoCharacterPath("/token/pos/");
            Assert.Equal("Characters.$.Token.pos", mongoPath);
        }

        [Fact]
        public void TestJsonPath2MongoPathForAttributes()
        {
            var mongoPath = translator.JsonPath2MongoCharacterPath("/attributes.description");
            Assert.Equal("Characters.$.Attributes.description", mongoPath);
        }

        [Fact]
        public void TestJsonPath2MongoPathHandlesKeywordsInPath()
        {
            var mongoPath = translator.JsonPath2MongoCharacterPath("/token/pos/token");
            Assert.Equal("Characters.$.Token.pos.token", mongoPath);
            mongoPath = translator.JsonPath2MongoCharacterPath("/token/pos/attributes");
            Assert.Equal("Characters.$.Token.pos.attributes", mongoPath);
        }

        [Fact]
        public void TestJsonPath2MongoPathForAssets()
        {
            var mongoPath = translator.JsonPath2MongoCharacterPath("/asset/src");
            Assert.Equal("Characters.$.Asset.src", mongoPath);
        }

        [Fact]
        public void TestJObjectToBson()
        {
            var value = translator.Json2Mongo(new JObject { { "q", 5 }, { "r", 6 } });
            Assert.Equal(new BsonDocument { { "q", 5 }, { "r", 6 } }, value);
        }

        [Fact]
        public void TestJsonValueToBson()
        {
            var value = translator.Json2Mongo(new JValue("string"));
            Assert.IsType<string>(value);
            Assert.Equal("string", value);
        }
    }
}