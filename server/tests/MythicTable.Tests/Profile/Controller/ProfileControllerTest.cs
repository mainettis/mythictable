using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Moq;
using MythicTable.Profile.Controller;
using MythicTable.Profile.Data;
using MythicTable.Profile.Exceptions;
using Xunit;

namespace MythicTable.Tests.Profile.Controller
{
    public class ProfileControllerTest : IAsyncLifetime
    {
        private ProfileController controller;
        private IProfileProvider provider;

        private string User { get; set; } = "jon@example.com";

        public Task InitializeAsync()
        {
            provider = new InMemoryProfileProvider(Mock.Of<ILogger<InMemoryProfileProvider>>());
            controller = new ProfileController(provider, new ConfigurationBuilder().Build());

            var mockHttpContext = new Mock<HttpContext>();
            mockHttpContext.Setup(hc => hc.User.FindFirst(It.IsAny<string>()))
                           .Returns(() => new Claim("", User));
            controller.ControllerContext.HttpContext = mockHttpContext.Object;
            return Task.CompletedTask;
        }

        public Task DisposeAsync()
        {
            return Task.CompletedTask;
        }

        [Fact]
        public async void MeCreatesProfile()
        {
            var allProfiles = await controller.Get(new List<string> { User });
            Assert.Empty(allProfiles);

            await controller.Me();

            allProfiles = await controller.Get(new List<string> { User });
            Assert.Single(allProfiles);
        }

        [Fact]
        public async void BasicFlow()
        {
            var result = await controller.Me();

            result.ImageUrl = "http://example.com/test.png";
            await controller.Put(result);

            var allProfiles = await controller.Get(new List<string>{ User });
            Assert.Single(allProfiles);
        }

        [Fact]
        public async void MePopulatesUserId()
        {
            var result = await controller.Me();
            Assert.Equal("jon", result.DisplayName);
        }

        [Fact]
        public async void MePopulatesDisplayName()
        {
            var result = await controller.Me();
            Assert.Equal(User, result.UserId);
        }

        [Fact]
        public async void UpdateRequiresSameUserIs()
        {
            var profile = new ProfileDto
            {
                UserId = $"Not {User}",
                DisplayName = "Test"
            };
            var exception = await Assert.ThrowsAsync<ProfileNotAuthorizedException>(() => controller.Put(profile));
            Assert.Equal($"User: '{User}' is not authorized to update profile for user: '{profile.UserId}'", exception.Message);
        }
    }
}