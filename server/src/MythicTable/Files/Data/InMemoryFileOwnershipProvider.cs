﻿using MythicTable.Files.Controllers;
using MythicTable.Files.Exceptions;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;


namespace MythicTable.Files.Data
{
    public class InMemoryFileOwnershipProvider : IFileOwnershipProvider
    {
        private long nextId = 1;
        private Dictionary<long, FileDto> files = new Dictionary<long, FileDto>();
        private Dictionary<string, List<FileDto>> userLists = new Dictionary<string, List<FileDto>>();

        public async Task<FileDto> Delete(string id, string userId)
        {
            var dto = await Get(id, userId);
            this.files.Remove(long.Parse(id));
            return dto;
        }

        public Task<FileDto> Get(string id, string userId)
        {
            long longId = long.Parse(id);
            FileDto dto = this.files.GetValueOrDefault(longId);
            if(dto == null)
            {
                throw new FileStorageException($"Could not find File of Id: '{id}'", HttpStatusCode.NotFound);
            }
            if (dto.User != userId)
            {
                throw new FileStorageException($"File '{id}' does not belong to user '{userId}'", HttpStatusCode.Forbidden);
            }
            return Task.FromResult(dto);
        }

        public Task<List<FileDto>> GetAll(string userId)
        {
            var userFiles = this.userLists.GetValueOrDefault(userId);
            if (userFiles == null)
            {
                userFiles = new List<FileDto>();
            }
            return Task.FromResult(userFiles);
        }

        public Task<List<FileDto>> Filter(string userId, FileFilter filter)
        {
            var userFiles = this.userLists.GetValueOrDefault(userId);
            if (userFiles == null)
            {
                return Task.FromResult(new List<FileDto>());
            }
            if (filter == null || filter.Path == null || filter.Path == string.Empty)
            {
                return Task.FromResult(userFiles);
            }
            return Task.FromResult(userFiles.Where(file => file.Path == filter.Path).ToList());
        }

        public Task<FileDto> Create(FileDto dto)
        {
            dto.Id = this.nextId.ToString();
            files.Add(this.nextId, dto);
            this.nextId++;
            var userFiles = this.userLists.GetValueOrDefault(dto.User);
            if (userFiles == null)
            {
                userFiles = new List<FileDto>();
                this.userLists[dto.User] = userFiles;
            }
            userFiles.Add(dto);
            return Task.FromResult(dto);
        }
    }
}
