using MythicTable.Common.Exceptions;

namespace MythicTable.Campaign.Exceptions
{
    public class CollectionNotFoundException : MythicTableException
        {
            public CollectionNotFoundException(string msg): base(msg) {}
        }
}