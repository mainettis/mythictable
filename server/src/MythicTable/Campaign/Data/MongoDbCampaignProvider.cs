using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.JsonPatch;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using MythicTable.Campaign.Exceptions;
using MythicTable.Common.JsonPatch;

namespace MythicTable.Campaign.Data
{
    public class MongoDbCampaignProvider : ICampaignProvider
    {
        private readonly IMongoCollection<CampaignDTO> campaigns;
        private readonly IMongoCollection<CampaignCharacterContainer> campaignCharacters;
        private readonly IMongoCollection<CampaignMessageContainer> campaignMessages;

        private readonly JsonPatchTranslator translator = new JsonPatchTranslator();

        public MongoDbCampaignProvider(MongoDbSettings settings, IMongoClient client)
        {
            var database = client.GetDatabase(settings.DatabaseName);
            campaigns = database.GetCollection<CampaignDTO>("campaign");
            campaignCharacters = database.GetCollection<CampaignCharacterContainer>("campaign-characters");
            campaignMessages = database.GetCollection<CampaignMessageContainer>("campaign-messages");
        }
        
        public async Task<List<CampaignDTO>> GetAll()
        {
            return await campaigns.Find(campaign => true).ToListAsync();
        }

        public async Task<CampaignDTO> Get(string campaignId)
        {
            var campaign = await campaigns.Find<CampaignDTO>(campaign => campaign.Id == campaignId).FirstOrDefaultAsync();
            if (campaign == null)
            {
                throw new CampaignNotFoundException($"Cannot find campaign of id {campaignId}");
            }

            return campaign;
        }

        public async Task<CampaignDTO> Create(CampaignDTO campaign, string owner)
        {
            if (campaign == null)
            {
                throw new CampaignInvalidException($"The campaign is null");
            }

            if (campaign.Id != null && campaign.Id.Length != 0)
            {
                throw new CampaignInvalidException($"The Campaign already has an id");
            }

            campaign.Owner = owner;
            await campaigns.InsertOneAsync(campaign);
            await campaignCharacters.InsertOneAsync(new CampaignCharacterContainer{
                Id = campaign.Id,
                Characters = new List<CharacterDTO>()
            });
            await campaignMessages.InsertOneAsync(new CampaignMessageContainer{
                Id = campaign.Id,
                Messages = new List<MessageDto>()
            });
            return campaign;
        }

        public async Task<CampaignDTO> Update(CampaignDTO campaign)
        {
            if (campaign == null)
            {
                throw new CampaignInvalidException($"The campaign is null");
            }

            if (campaign.Id == null || campaign.Id.Length == 0)
            {
                throw new CampaignInvalidException($"The Campaign MUST have an id");
            }

            await campaigns.ReplaceOneAsync(c => c.Id == campaign.Id, campaign);
            return campaign;
        }

        public async Task Delete(string campaignId)
        {
            var campaign = await this.Get(campaignId);
            var results = await campaigns.DeleteOneAsync(campaign => campaign.Id == campaignId);
            if (results.DeletedCount == 0) 
            {
                new CampaignNotFoundException($"Campaign id {campaignId} doesn't exist");
            }
        }

        public async Task<List<PlayerDTO>> GetPlayers(string campaignId)
        {
            var campaign = await this.Get(campaignId);

            if (campaign == null)
            {
                throw new CampaignNotFoundException($"Get Player. Cannot find campaign of id {campaignId}");
            }

            return campaign.Players;
        }
        
        public async Task<CampaignDTO> AddPlayer(string campaignId, PlayerDTO player)
        {
            var campaign = await this.Get(campaignId);
            
            if (campaign == null)
            {
                throw new CampaignNotFoundException($"Add Player. Cannot find campaign of id {campaignId}");
            }
            
            if (campaign.Players.Any(m => m.Name == player.Name))
            {
                throw new CampaignAddPlayerException($"The player '{player.Name}' is already in campaign {campaignId}");
            }

            campaign.Players.Add(new PlayerDTO
            {
                Name = player.Name
            });
            
            await this.Update(campaign);
            return campaign;
        }

        public async Task<CampaignDTO> RemovePlayer(string campaignId, PlayerDTO player)
        {
            var campaign = await this.Get(campaignId);
            if (campaign == null || campaign.Players == null)
            {
                throw new CampaignNotFoundException($"Remove Player. Cannot find campaign of id {campaignId}");
            }

            var numberRemoved = campaign.Players.RemoveAll(membership => membership.Name == player.Name);
            if (numberRemoved == 0)
            {
                throw new CampaignRemovePlayerException($"The player '{player.Name}' is not in campaign {campaignId}");
            }

            await this.Update(campaign);
            return campaign;
        }

        public async Task<List<CharacterDTO>> GetCharacters(string campaignId)
        {
            var campaignCharacter = await campaignCharacters.Find<CampaignCharacterContainer>(cc => cc.Id == campaignId).FirstOrDefaultAsync();
            if (campaignCharacter == null)
            {
                return new List<CharacterDTO>();
            }
            return campaignCharacter.Characters;
        }

        public async Task<CharacterDTO> AddCharacter(string campaignId, CharacterDTO character)
        {
            var campaignCharacter = await campaignCharacters.Find<CampaignCharacterContainer>(cc => cc.Id == campaignId).FirstOrDefaultAsync();
            if (campaignCharacter == null)
            {
                await campaignCharacters.InsertOneAsync(new CampaignCharacterContainer{
                    Id = campaignId,
                    Characters = new List<CharacterDTO>{
                        character
                    }
                });
            }
            else
            {
                character.Id = ObjectId.GenerateNewId().ToString();
                await campaignCharacters.UpdateOneAsync(
                    c => c.Id == campaignId,
                    Builders<CampaignCharacterContainer>.Update.Push("Characters", character));
            }
            return character;
        }

        public async Task<List<CharacterDTO>> AddCharacters(string campaignId, List<CharacterDTO> newCharacters)
        {
            var campaignCharacter = await campaignCharacters.Find<CampaignCharacterContainer>(cc => cc.Id == campaignId).FirstOrDefaultAsync();
            foreach(var c in newCharacters)
            {
                c.Id = ObjectId.GenerateNewId().ToString();
            }
            if (campaignCharacter == null)
            {
                await campaignCharacters.InsertOneAsync(new CampaignCharacterContainer{
                    Id = campaignId,
                    Characters = newCharacters
                });
            }
            else
            {
                await campaignCharacters.UpdateOneAsync(
                    c => c.Id == campaignId,
                    Builders<CampaignCharacterContainer>.Update.PushEach("Characters", newCharacters));
            }
            return newCharacters;
        }

        public async Task RemoveCharacter(string campaignId, string characterId)
        {
            var update = Builders<CampaignCharacterContainer>
                .Update
                .PullFilter(
                    campaignContainer => campaignContainer.Characters,
                    character => character.Id == characterId);
            var result = await campaignCharacters.UpdateOneAsync(campaignContainer => campaignContainer.Id == campaignId, update);
            if(result.ModifiedCount == 0)
            {
                throw new CharacterNotRemovedException($"Could not remove character with id '{characterId}' from campaign ${campaignId}");
            }
        }

        public async Task<long> MoveCharacter(string campaignId, string characterId, double x, double y)
        {
            var filter = Builders<CampaignCharacterContainer>.Filter.Where(x => x.Id == campaignId && x.Characters.Any(i => i.Id == characterId));
            var update = Builders<CampaignCharacterContainer>.Update
                .Set("Characters.$.Token.pos.q", x)
                .Set("Characters.$.Token.pos.r", y);
            var result = await campaignCharacters.UpdateOneAsync(filter, update);
            return result.ModifiedCount;
        }

        public async Task<long> UpdateCharacter(string campaignId, string characterId, JsonPatchDocument patch)
        {
            var filter = Builders<CampaignCharacterContainer>.Filter.Where(x => x.Id == campaignId && x.Characters.Any(i => i.Id == characterId));
            var update = Builders<CampaignCharacterContainer>.Update.Set(translator.JsonPath2MongoCharacterPath(patch.Operations[0].path), translator.Json2Mongo(patch.Operations[0].value));
            for (int i = 1; i < patch.Operations.Count; i++)
            {
                var operation = patch.Operations[i];
                update = update.Set(translator.JsonPath2MongoCharacterPath(operation.path), translator.Json2Mongo(operation.value));
            }
            var result = await campaignCharacters.UpdateOneAsync(filter, update);
            return result.ModifiedCount;
        }

        public async Task<List<MessageDto>> GetMessages(string campaignId)
        {
            var campaignMessageContainer = await campaignMessages.Find<CampaignMessageContainer>(cc => cc.Id == campaignId).FirstOrDefaultAsync();
            if (campaignMessageContainer == null)
            {
                return new List<MessageDto>();
            }
            return campaignMessageContainer.Messages;
        }
        
        public async Task<MessageDto> AddMessage(string campaignId, MessageDto message)
        {
            var campaignMessageContainer = await campaignMessages.Find<CampaignMessageContainer>(cc => cc.Id == campaignId).FirstOrDefaultAsync();
            if (campaignMessageContainer == null)
            {
                await campaignMessages.InsertOneAsync(new CampaignMessageContainer{
                    Id = campaignId,
                    Messages = new List<MessageDto>{
                        message
                    }
                });
            }
            else
            {
                message.Id = ObjectId.GenerateNewId().ToString();
                await campaignMessages.UpdateOneAsync(
                    c => c.Id == campaignId,
                    Builders<CampaignMessageContainer>.Update.Push("Messages", message));
            }
            return message;
        }

        private class CampaignCharacterContainer
        {
            [BsonId]
            [BsonRepresentation(BsonType.ObjectId)]     
            public string Id { get; set; }
            public List<CharacterDTO> Characters { get; set; } = new List<CharacterDTO>();
        }

        private class CampaignMessageContainer
        {
            [BsonId]
            [BsonRepresentation(BsonType.ObjectId)]     
            public string Id { get; set; }
            public List<MessageDto> Messages { get; set; } = new List<MessageDto>();
        }
    }
}
