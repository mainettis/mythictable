using System;
using System.Collections.Generic;
using Mongo.Migration.Documents;
using Mongo.Migration.Documents.Attributes;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace MythicTable.Campaign.Data
{
    public class CampaignDTO
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]     
        public string Id { get; set; }
        public string Owner { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ImageUrl { get; set; }
        public DateTime Created { get; set; }
        public DateTime LastModified { get; set; }

        public List<PlayerDTO> Players { get; set; } = new List<PlayerDTO>();
    }

    [RuntimeVersion("0.0.1")]
    public class CampaignCharacterContainer : IDocument
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public List<CharacterDTO> Characters { get; set; } = new List<CharacterDTO>();

        public DocumentVersion Version { get; set; }
    }

    public class CampaignMessageContainer
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public List<MessageDto> Messages { get; set; } = new List<MessageDto>();
    }
}
