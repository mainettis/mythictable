﻿using Mongo.Migration.Migrations;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MythicTable.Campaign.Data.Migrations.Character
{
    public class M001_CharacterDtoToJson : Migration<CharacterDTO>
    {
        public M001_CharacterDtoToJson()
            : base("0.0.1")
        {
        }

        public override void Up(BsonDocument document)
        {
            document.Set("Token", document["token"]);
            document.Remove("token");
            document.Set("Asset", document["asset"]);
            document.Remove("asset");
        }

        public override void Down(BsonDocument document)
        {
        }
    }
}
