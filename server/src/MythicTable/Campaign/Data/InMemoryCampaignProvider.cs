using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.JsonPatch;
using MythicTable.Campaign.Exceptions;
using MythicTable.Common.Data;

namespace MythicTable.Campaign.Data
{
    public class InMemoryCampaignProvider : ICampaignProvider
    {
        private Dictionary<string, CampaignDTO> campaigns;
        private Dictionary<string, List<CharacterDTO>> characters = new Dictionary<string,List<CharacterDTO>>();
        private Dictionary<string, List<MessageDto>> messages = new Dictionary<string,List<MessageDto>>();

        public InMemoryCampaignProvider()
        {
            campaigns = new Dictionary<string,CampaignDTO>();
        }
        public Task<List<CampaignDTO>> GetAll()
        {            
            return Task.FromResult(campaigns.Values.ToList<CampaignDTO>());
        }

        public Task<CampaignDTO> Get(string campaignId)
        {
            CampaignDTO campaign;
            campaigns.TryGetValue(campaignId,out campaign);
            if (campaign == null)
            {
                throw new CampaignNotFoundException($"Cannot find campaign of id {campaignId}");
            }

            return Task.FromResult(campaign);
        }

        public Task<CampaignDTO> Create(CampaignDTO campaign, string owner)
        {
            if (campaign == null)
            {
                throw new CampaignInvalidException($"The campaign is null");
            }

            if (campaign.Id != null && campaign.Id.Length != 0)
            {
                throw new CampaignInvalidException($"The Campaign already has an id");
            }

            campaign.Owner = owner;
            campaign.Id = Guid.NewGuid().ToString();
            this.campaigns[campaign.Id] = campaign;
            this.characters.Add(campaign.Id, new List<CharacterDTO>());
            this.messages.Add(campaign.Id, new List<MessageDto>());
            return Task.FromResult(campaign);
        }

        public Task<CampaignDTO> Update(CampaignDTO campaign)
        {
            if (campaign == null)
            {
                throw new CampaignInvalidException($"The campaign is null");
            }

            if (campaign.Id == null || campaign.Id.Length == 0)
            {
                throw new CampaignInvalidException($"The Campaign MUST have an id");
            }

            campaigns[campaign.Id] = campaign;
            return Task.FromResult(campaign);
        }

        public Task Delete(string campaignId)
        {
            var campaign =  this.Get(campaignId);
            if (campaign == null) 
            {
                new CampaignNotFoundException($"Campaign id {campaignId} doesn't exist");
            }
            campaigns.Remove(campaignId);
            return Task.CompletedTask;
        }

        public async Task<List<PlayerDTO>> GetPlayers(string campaignId)
        {
            var campaign = await this.Get(campaignId);

            if (campaign == null)
            {
                throw new CampaignNotFoundException($"Get Player. Cannot find campaign of id {campaignId}");
            }

            return campaign.Players;
        }
        
        public async Task<CampaignDTO> AddPlayer(string campaignId, PlayerDTO player)
        {
            var campaign = await this.Get(campaignId);
            
            if (campaign == null)
            {
                throw new CampaignNotFoundException($"Add Player. Cannot find campaign of id {campaignId}");
            }
            
            if (campaign.Players.Any(m => m.Name == player.Name))
            {
                throw new CampaignAddPlayerException($"The player '{player.Name}' is already in campaign {campaignId}");
            }

            campaign.Players.Add(new PlayerDTO
            {
                Name = player.Name
            });
            
            await this.Update(campaign);
            return campaign;
        }

        public Task<CampaignDTO> RemovePlayer(string campaignId, PlayerDTO player)
        {
            var campaign =  this.Get(campaignId).Result;
            if (campaign == null || campaign.Players == null)
            {
                throw new CampaignNotFoundException($"Remove Player. Cannot find campaign of id {campaignId}");
            }

            var numberRemoved = campaign.Players.RemoveAll(membership => membership.Name == player.Name);
            if (numberRemoved == 0)
            {
                throw new CampaignRemovePlayerException($"The player '{player.Name}' is not in campaign {campaignId}");
            }

            this.Update(campaign);
            return Task.FromResult(campaign);
        }

        public Task<List<CharacterDTO>> GetCharacters(string campaignId)
        {
            if(!this.characters.ContainsKey(campaignId))
            {
                return Task.FromResult(new List<CharacterDTO>());
            }
            return Task.FromResult(characters[campaignId]);
        }

        public Task<CharacterDTO> AddCharacter(string campaignId, CharacterDTO character)
        {
            if(!this.characters.ContainsKey(campaignId))
            {
                this.characters.Add(campaignId, new List<CharacterDTO>());
            }
            var nextIndex = this.characters[campaignId].Count + 1;
            character.Id = nextIndex.ToString();
            this.characters[campaignId].Add(character);
            return Task.FromResult(character);
        }

        public Task<List<CharacterDTO>> AddCharacters(string campaignId, List<CharacterDTO> characters)
        {
            if(!this.characters.ContainsKey(campaignId))
            {
                this.characters.Add(campaignId, new List<CharacterDTO>());
            }
            var nextIndex = this.characters[campaignId].Count + 1;
            foreach(var c in characters)
            {
                c.Id = (nextIndex++).ToString();
            }
            this.characters[campaignId].AddRange(characters);
            return Task.FromResult(characters);
        }

        public Task RemoveCharacter(string campaignId, string characterId)
        {
            if(!this.characters.ContainsKey(campaignId))
            {
                throw new CampaignNotFoundException($"Could not find campaign with id '{campaignId}'");
            }

            if(!this.characters[campaignId].Exists(x => x.Id == characterId))
            {
                throw new CharacterNotFoundException($"Could not find character with id '{characterId}'");
            }

            foreach(var c in this.characters[campaignId])
            {
                if(c.Id == characterId)
                {
                    this.characters[campaignId].Remove(c);
                    break;
                }
            }

            return Task.CompletedTask;
        }

        public Task<long> MoveCharacter(string campaignId, string characterId, double x, double y)
        {
            CharacterDTO character = GetCharacter(campaignId, characterId);
            character.Token["pos"]["q"] = x;
            character.Token["pos"]["r"] = y;
            return Task.FromResult<long>(1);
        }

        public Task<long> UpdateCharacter(string campaignId, string characterId, JsonPatchDocument patch)
        {
            CharacterDTO character = GetCharacter(campaignId, characterId);
            foreach(var operation in patch.Operations)
            {
                if(operation.path.StartsWith("/token"))
                {
                    operation.path = new Regex("^/token").Replace(operation.path, "", 1);
                    character.Token = character.Token.Patch(operation);
                }
                if (operation.path.StartsWith("/asset"))
                {
                    operation.path = new Regex("^/asset").Replace(operation.path, "", 1);
                    character.Asset = character.Asset.Patch(operation);
                }
                if (operation.path.StartsWith("/attributes"))
                {
                    operation.path = new Regex("^/attributes").Replace(operation.path, "", 1);
                    character.Attributes = character.Attributes.Patch(operation);
                }
            }
            return Task.FromResult<long>(1);
        }

        public Task<List<MessageDto>> GetMessages(string campaignId)
        {
            if(!this.messages.ContainsKey(campaignId))
            {
                return Task.FromResult(new List<MessageDto>());
            }
            return Task.FromResult(messages[campaignId]);
        }
        
        public Task<MessageDto> AddMessage(string campaignId, MessageDto message)
        {
            if(!this.messages.ContainsKey(campaignId))
            {
                this.messages.Add(campaignId, new List<MessageDto>());
            }
            var nextIndex = this.messages[campaignId].Count + 1;
            message.Id = nextIndex.ToString();
            this.messages[campaignId].Add(message);
            return Task.FromResult(message);
        }

        private CharacterDTO GetCharacter(string campaignId, string characterId)
        {
            if(!this.characters.ContainsKey(campaignId))
            {
                throw new CampaignNotFoundException($"Cannot find character data for campaign of id {campaignId}");
            }
            CharacterDTO character = null;
            foreach(var c in this.characters[campaignId])
            {
                if(c.Id == characterId)
                {
                    character = c;
                    break;
                }
            }
            if(character == null)
            {
                throw new CharacterNotFoundException($"Cannot find character of id {characterId} in campaign {campaignId}");
            }
            return character;
        }
    }
}
