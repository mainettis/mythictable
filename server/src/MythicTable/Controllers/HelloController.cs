using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using MythicTable.Common.Extensions.Controllers;

namespace MythicTable.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HelloController : ControllerBase
    {
        private IConfiguration Configuration { get; }

        public HelloController(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        // GET: api/hello
        [HttpGet]
        public string Hello()
        {
            return "hello";
        }

        // GET: api/hello/me
        [HttpGet("me")]
        [Authorize]
        public string HelloMe()
        {
            return $"hello {this.GetUserId(Configuration)}";
        }
    }
}
