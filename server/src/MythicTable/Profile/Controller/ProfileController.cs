﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using MythicTable.Common.Extensions.Controllers;
using MythicTable.Profile.Data;
using MythicTable.Profile.Exceptions;

namespace MythicTable.Profile.Controller
{
    [Route("api/profiles")]
    [ApiController]
    public class ProfileController : ControllerBase
    {
        private readonly IProfileProvider provider;

        private IConfiguration Configuration { get; }

        public ProfileController(IProfileProvider provider, IConfiguration configuration)
        {
            this.provider = provider;
            Configuration = configuration;
        }

        [Authorize]
        [HttpGet("me")]
        public async Task<ProfileDto> Me()
        {
            var userId = this.GetUserId(Configuration);
            try
            {
                return await provider.Get(userId);
            }
            catch (ProfileNotFoundException)
            {
                // TODO - Come up with a better way create a display name 
                // https://gitlab.com/mythicteam/mythictable/-/issues/145
                var displayName = userId.Split("@")[0];
                return await provider.Create(new ProfileDto { DisplayName = displayName }, userId);
            }
        }

        [HttpGet("{userId}")]
        public async Task<ProfileDto> Get(string userId)
        {
            return await provider.Get(userId);
        }

        [HttpGet]
        public async Task<List<ProfileDto>> Get([FromQuery(Name = "userId")]  List<string> userIds)
        {
            return await provider.Get(userIds.ToArray());
        }

        [Authorize]
        [HttpPut()]
        public async Task<ProfileDto> Put(ProfileDto dto)
        {
            var user = this.GetUserId(Configuration);
            if (user != dto.UserId)
            {
                throw new ProfileNotAuthorizedException($"User: '{user}' is not authorized to update profile for user: '{dto.UserId}'");
            }
            return await provider.Update(dto);
        }
    }
}
