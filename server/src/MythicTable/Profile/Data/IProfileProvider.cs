using System.Collections.Generic;
using System.Threading.Tasks;

namespace MythicTable.Profile.Data
{
    public interface IProfileProvider
    {
        Task<ProfileDto> Get(string userId);
        Task<List<ProfileDto>> Get(string[] userId);
        Task<ProfileDto> Create(ProfileDto profile, string userId);
        Task<ProfileDto> Update(ProfileDto profile);
        Task Delete(string userId);
    }
}