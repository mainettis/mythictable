﻿using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace MythicTable.TextParsing
{
    public class ChatParser
    {
        private readonly IRoller roller;

        public ChatParser(IRoller roller = null)
        {
            this.roller = roller;
        }

        public ChatParserResults Process(string text)
        {
            return new ChatParserResults { Elements = Parse(text) };
        }

        public IEnumerable<Element> Parse(string text)
        {
            if (roller != null && roller.IsFormula(text))
            {
                return new List<Element> { new RollerElement { Results = roller.Roll(text) } };
            }

            List<Element> results = new List<Element>();

            string pattern = @"(\[\[[^\[]+\]\])";
            var numRolls = 0;

            foreach (string result in Regex.Split(text, pattern))
            {
                if(roller != null && roller.CanRoll(result))
                {
                    results.Add(new RollerElement { Results = roller.Roll(result), Index = numRolls++ });
                }
                else
                {
                    results.Add(new TextElement { Text = result });
                }
            }
            return results;
        }
    }
}