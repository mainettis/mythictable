﻿using Dice;

namespace MythicTable.TextParsing
{
    public interface Element
    {
        public string Text { get; }
        public string Description { get; }
    }

    public class TextElement: Element
    {
        public string Text { get; set; }
        public string Description { get; } = null;
    }

    public class RollerElement : Element
    {
        public RollResult Results { get; set; }
        public int Index { get; set; }

        public string Text => $"[{Results.Value}|{Index}]";

        public string Description => Results.ToString();
    }
}