﻿using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MythicTable.TextParsing
{
    public class ChatParserResults
    {
        public IEnumerable<Element> Elements { get; set; }

        public string Text
        {
            get { return string.Join("", Elements.Select(e => e.Text)); }
        }

        public string Description
        {
            get { return string.Join(", ", Elements.Select(e => e.Description).Where(d => d != null)); }
        }

        public List<DiceDto> Dice
        {
            get
            {
                return Elements.Where(e => e is RollerElement).Select(e => new DiceDto(e as RollerElement)).ToList();
            }
        }

        public ChatDto AsDto()
        {
            return new ChatDto {
                Message = Text,
                Description = Description,
                Dice = Dice
            };
        }
    }

    public class ChatDto
    {
        private string message;
        private string description;

        public string Message
        {
            get => message;
            set => message = HttpUtility.HtmlEncode(value);
        }

        public string Description
        {
            get => description;
            set => description = HttpUtility.HtmlEncode(value);
        }

        public List<DiceDto> Dice { get; set; }
    }

    public class DiceDto
    {
        public decimal Result { get; set; }
        public string Formula { get; set; }
        public List<DieDto> Rolls { get; set; }

        public DiceDto()
        {
        }

        public DiceDto(RollerElement element)
        {
            Result = element.Results.Value;
            Formula = element.Results.Expression;
            Rolls = element.Results.Values
                        .Where(dr => dr.NumSides > 0) // This is a bug in the SkizzerzRoller where we get extra dice with 0 sides occasionally
                        .Select(dr => new DieDto { Die = dr.NumSides, Value = dr.Value}).ToList();
        }
    }

    public class DieDto
    {
        public int Die { get; set; }
        public decimal Value { get; set; }
    }
}
