﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Logging;
using MythicTable.Campaign.Data;
using MythicTable.Campaign.Util;
using MythicTable.GameSession;
using MythicTable.Collections.Providers;
using Newtonsoft.Json.Linq;
using MythicTable.Collections.Data;
using Microsoft.Extensions.Configuration;
using MythicTable.TextParsing;

namespace MythicTable
{
    public class LivePlayHub : Hub<ILiveClient>
    {
        private ICampaignProvider CampaignProvider { get; }
        private ICollectionProvider CollectionProvider { get; }
        private IConfiguration Configuration { get; }
        private readonly ILogger logger;

        private readonly ChatParser parser;

        public LivePlayHub(
            ICampaignProvider campaignProvider,
            ICollectionProvider collectionProvider,
            IConfiguration configuration,
            ILogger<LivePlayHub> logger)
        {
            CampaignProvider = campaignProvider;
            CollectionProvider = collectionProvider;
            Configuration = configuration;
            this.logger = logger;
            parser = new ChatParser(new SkizzerzRoller());
        }

        // TODO: #17: A difference between the GameState on the server and the GameState for player clients exists. This means a delta should not just be broadcast once applied
        // The GM client will resolve actions and submit 1 delta for the players and another delta for the server
        [Authorize]
        [HubMethodName("submitDelta")]
        public async Task<bool> UpdateCharacter(CharacterDelta delta)
        {
            var success = await InternalUpdateCharacter(delta);
            
            if (success)
            {
                await Clients.All.ConfirmDelta(delta);
            }

            return success;
        }

        [Authorize]
        public async Task<CharacterDTO> AddCharacter(AddCharacterRequest request)
        {
            var character = CharacterUtil.CreateCharacter(request.sceneId, request.image, request.x, request.y);
            character = await CampaignProvider.AddCharacter(request.campaignId, character);
            await Clients.All.CharacterAdded(character);
            return character;
        }

        [Authorize]
        public async Task<bool> RemoveCharacter(RemoveCharacterRequest request)
        {
            await CampaignProvider.RemoveCharacter(request.CampaignId, request.CharacterId);
            await Clients.All.CharacterRemoved(request.CharacterId);
            return true;
        }

        [Authorize]
        public async Task<bool> SendMessage(MessageDto message)
        {
            var results = parser.Process(message.Message);
            message.Result = results.AsDto();
            this.logger.LogInformation($"Dice Roll - User: {message.UserId} Roll: {message.Message} Results: {message.Result.Dice} Message: {message.Result.Message}");
            var campaignId = message.SessionId;
            await CampaignProvider.AddMessage(campaignId, message);
            await Clients.All.SendMessage(message);
            return true;
        }

        // TODO - Delete this
        [Authorize]
        [HubMethodName("submitDeltaTemp")]
        public async Task<bool> RebroadcastDeltaTemp(SessionOpDelta delta)
        {
            await Clients.All.ConfirmOpDelta(delta);
            return true;
        }

        [Authorize]
        public async Task<bool> DrawLine(JObject lineData)
        {
            await Clients.All.DrawLine(lineData);
            return true;
        }

        [Authorize]
        public async Task<JObject> AddCollectionItem(string collection, string campaignId, JObject item)
        {
            var obj = await CollectionProvider.CreateByCampaign(this.GetUserId(), collection, campaignId, item);
            await Clients.All.ObjectAdded(collection, obj);
            return obj;
        }

        [Authorize]
        public async Task<JObject> UpdateObject(UpdateCollectionHubParameters parameters)
        {
            if (await CollectionProvider.UpdateByCampaign(parameters.Collection, parameters.CampaignId, parameters.Id, parameters.Patch) > 0)
            {
                await Clients.All.ObjectUpdated(parameters);
                return await CollectionProvider.GetByCampaign(parameters.Collection, parameters.CampaignId, parameters.Id);
            }
            return null;
        }

        [Authorize]
        public async Task<bool> RemoveObject(string collection, string id)
        {
            if (await CollectionProvider.Delete(this.GetUserId(), collection, id) > 0)
            {
                await Clients.All.ObjectRemoved(collection, id);
                return true;
            }
            return false;
        }

        private async Task<bool> InternalUpdateCharacter(CharacterDelta delta)
        {
            var campaignId = delta.CampaignId;
            foreach(var entity in delta.Entities)
            {
                this.logger.LogInformation($"Entity: {entity.EntityId}");
                await this.CampaignProvider.UpdateCharacter(campaignId, entity.EntityId, entity.Patch);
            }
            return true;
        }

        private string GetUserId()
        {
            if (Configuration.GetValue<bool>("MTT_USE_KEYCLOAK"))
            {
                return this.Context.User.FindFirst("preferred_username")?.Value;
            }
            else
            {
                return this.Context.User.FindFirst("name")?.Value;
            }
        }
    }
}
