﻿using System.Threading.Tasks;
using MythicTable.Campaign.Data;
using MythicTable.Collections.Data;
using Newtonsoft.Json.Linq;

namespace MythicTable.GameSession
{
    public interface ILiveClient
    {
        Task ConfirmDelta(CharacterDelta delta);
        Task ConfirmOpDelta(SessionOpDelta delta);
        Task CharacterAdded(CharacterDTO character);
        Task CharacterRemoved(string characterId);

        Task SendMessage(MessageDto message);

        Task ObjectAdded(string collection, JObject obj);
        Task ObjectUpdated(UpdateCollectionHubParameters parameters);
        Task ObjectRemoved(string collection, string id);

        Task ExceptionRaised(string exception);

        Task DrawLine(JObject obj);
    }
}
