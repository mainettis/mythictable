# Mythic Table - Webapp

The Mythic Table frontend is a Vue.js single page app. See the main [README.md](../README.md) for setup instructions.

## Architecture

TODO

## Development

Development of the Mythic Table webapp requires node 12.17.0 or later within the 12 series. Higher and lower major versions of node may have inconsistent test and build results.

## Testing

TODO

## Directory Layout

Within the `html` directory, the layout is as follows:

Root of `html/` - Files used to bootstrap the app, and the following directories.

`dist/` - Reserved for build artifacts.

`node_modules/` - Reserved for dependencies.

`public/` - Files used to serve the built app.

`public/static/` - Files such as images and fonts to be served with the app.

`src/` - Scaffolding for the app.

`src/components/` - Contains directories for each feature. A feature should be a self-contained bit of user functionality usually does not need to import from any other feature. A feature may otherwise import only from `utils`, `common`, and `core`. No files should be directly under `components`.

`src/components/common/` - Contains directories for shared visual components. A shared visual component must not import from a feature, but may import from `core`, `util`, and other `common` components. No files should be directly under `common`.

`src/core/` - Non-visual business logic functionality. May import from anywhere except `views`. May contain files or directories.

`src/store/` - Data management. May import from `core`, `utils`, and feature-specific stores.

`src/utils/` - Non-business logic shared functionality. May only import from other utils. May contain files or directories.

`src/views/` - Components that represent the combination of features that constitutes a route. May import from anywhere. May contain files or directories.

`tests/` - Test files. The directory structure mirrors that of `src` exactly, so that the path of a given test file is the same as that of the file it tests but with `src` replaced with `tests`.
