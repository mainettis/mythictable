const DrawingStore = {
    namespaced: true,
    state: {
        active: false,
        receivedLines: [],
        drawerLastActive: {},
    },
    actions: {
        toggle({ state }) {
            state.active = !state.active;
        },
        drawLine({ state, rootState }, { sceneId, line }) {
            state.receivedLines.push(line);
            rootState.live.director.drawLine(sceneId, line);
            console.log(state.receivedLines);
        },
        lineDrawReceived({ state, rootState }, { line }) {
            // This artist was active
            state.drawerLastActive[line.attrs.name] = Date.now();
            console.log(state.drawerLastActive);
            // Make sure it's not one of ours we've already saved
            if (rootState.live.userId !== line.attrs.name) {
                state.receivedLines.push(line);
            }
            console.log(state.receivedLines);
        },
        purgeUsersLines({ state }, { userId }) {
            state.receivedLines = state.receivedLines.filter(line => line.attrs.name !== userId);
            delete state.drawerLastActive[userId];
            console.log(userId, state.receivedLines, state.drawerLastActive);
        },
    },
};

export default DrawingStore;
