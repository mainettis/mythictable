import Vue from 'vue';
import jsonpatch from 'fast-json-patch';
import { getByCampaign } from './api';
import { COLLECTION_TYPES } from './constants';
import Asset from '@/core/entity/Asset';

function getCollection(state, collection) {
    if (state.hasOwnProperty(collection)) {
        return state[collection];
    }
    return [];
}

function getItem(state, collection, id) {
    const c = getCollection(state, collection);
    if (c.hasOwnProperty(id)) {
        return c[id];
    }
    return null;
}

async function loadCollection(commit, rootState, collection) {
    const results = await getByCampaign(collection, rootState.live.sessionId);
    for (const i in results) {
        commit('add', { collection, item: results[i] });
    }
    return results;
}

const CollectionStore = {
    namespaced: true,
    state: {},
    getters: {
        getDirector(state, getters, rootState) {
            return rootState.live.director;
        },
        getItem: state => (collection, id) => {
            return getItem(state, collection, id);
        },
        getCollection: state => collection => {
            return getCollection(state, collection);
        },
        scenes: state => () => {
            return getCollection(state, COLLECTION_TYPES.scenes);
        },
    },
    mutations: {
        patch(state, { collection, id, patch }) {
            const item = getItem(state, collection, id);
            jsonpatch.applyPatch(item, patch);
            if (patch.filter(p => p.op == 'add')) {
                Vue.delete(state[collection], id);
                Vue.set(state[collection], id, item);
            }
        },
        add(state, { collection, item }) {
            if (!state.hasOwnProperty(collection)) {
                Vue.set(state, collection, {});
            }
            Vue.set(state[collection], item._id, item);
        },
        remove(state, { collection, id }) {
            if (state.hasOwnProperty(collection)) {
                Vue.delete(state[collection], id);
            }
        },
    },
    actions: {
        async update({ getters }, { collection, id, patch }) {
            await getters.getDirector.updateCampaignObject(collection, id, patch);
        },
        async add({ getters }, { collection, item }) {
            await getters.getDirector.addCampaignObject(collection, item);
        },
        async remove({ getters }, { collection, id }) {
            await getters.getDirector.removeCampaignObject(collection, id);
        },
        async load({ commit, rootState }, { collection }) {
            return await loadCollection(commit, rootState, collection);
        },

        async onAdded({ commit }, { collection, item }) {
            commit('add', { collection, item });
            if (collection == COLLECTION_TYPES.scenes) {
                await Asset.updateAll(item.stage.elements.map(e => e.asset));
            }
        },
        async onRemoved({ commit }, { collection, id }) {
            commit('remove', { collection, id });
        },
        async onUpdated({ commit, state }, parameters) {
            commit('patch', parameters);
            if (parameters.collection == COLLECTION_TYPES.scenes) {
                const item = getItem(state, parameters.collection, parameters.id);
                await Asset.updateAll(item.stage.elements.map(e => e.asset));
            }
        },
        async onLoad({ commit }, { collection, items }) {
            for (const i in items) {
                commit('add', { collection, item: items[i] });
            }
        },

        async loadScenes({ commit, rootState }) {
            return await loadCollection(commit, rootState, COLLECTION_TYPES.scenes);
        },
    },
};

export default CollectionStore;
