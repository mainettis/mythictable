import Vue from 'vue';
import ProfileApi from '@/core/profile/api';
import { addFile, FILE_TYPES } from '@/core/api/files/files.js';

const ProfileStore = {
    namespaced: true,
    state: {
        me: {},
        showEditor: false,
    },
    mutations: {
        me(state, profile) {
            Vue.set(state, 'me', profile);
        },
        updateImage(state, image) {
            Vue.set(state.me, 'imageUrl', image);
        },
    },
    actions: {
        async me({ commit }) {
            const me = await ProfileApi.me();
            commit('me', me);
            return me;
        },
        edit({ state }, show) {
            state.showEditor = show;
        },
        async uploadImage({ commit }, event) {
            let files;
            files = event && event.target && event.target.files;
            const results = await addFile(files, FILE_TYPES.PROFILE);
            console.log(JSON.stringify(results.data));
            if (results.data.count > 0) {
                commit('updateImage', results.data.files[0].url);
            }
        },
        async update({ commit }, profile) {
            const me = await ProfileApi.update(profile);
            commit('me', me);
            return me;
        },
    },
};

export default ProfileStore;
