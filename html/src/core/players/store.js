import { COLLECTION_TYPES } from '@/core/collections/constants';

export const getters = {
    getPlayer: (state, getters, rootState, rootGetters) => id => {
        return rootGetters['collections/getItem'](COLLECTION_TYPES.players, id);
    },
    getPlayers: (state, getters, rootState, rootGetters) => () => {
        return rootGetters['collections/getCollection'](COLLECTION_TYPES.players);
    },
};

export const actions = {
    async add({ dispatch }, { player }) {
        return await dispatch(
            'collections/add',
            { collection: COLLECTION_TYPES.players, item: player },
            { root: true },
        );
    },
    async update({ dispatch }, parameters) {
        return await dispatch(
            'collections/update',
            { collection: COLLECTION_TYPES.players, ...parameters },
            { root: true },
        );
    },
    async load({ dispatch }) {
        return await dispatch('collections/load', { collection: COLLECTION_TYPES.players }, { root: true });
    },
};

const PlayerStore = {
    namespaced: true,
    getters,
    actions,
};

export default PlayerStore;
