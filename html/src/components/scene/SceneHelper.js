export default function getAllImages(scenes) {
    let images = {};
    scenes.forEach(scene => {
        scene.stage.elements.forEach(element => {
            images[scene._id] = {
                id: element.id,
                asset: element.asset,
            };
        });
    });
    return images;
}
