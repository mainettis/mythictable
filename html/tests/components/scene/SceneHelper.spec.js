import getAllImages from '@/components/scene/SceneHelper.js';

const scenes = [
    {
        _id: 'debug',
        scene: { stage: '.' },
        stage: {
            grid: { type: 'square', size: 50 },
            bounds: {
                nw: { q: -1, r: -1 },
                se: { q: 29, r: 20 },
            },
            color: '#223344',
            elements: [
                {
                    id: 'background',
                    asset: { kind: 'image', src: '/static/assets/hideout.png' },
                    pos: { q: 0, r: 0, pa: '00' },
                },
            ],
        },
    },
];

const expectedImages = {
    debug: {
        id: 'background',
        asset: { kind: 'image', src: '/static/assets/hideout.png' },
    },
};

describe('GameMat component', () => {
    describe('scene conversion', () => {
        it('should pull images from scene description', () => {
            const images = getAllImages(scenes);
            expect(images).toEqual(expectedImages);
        });
    });
});
